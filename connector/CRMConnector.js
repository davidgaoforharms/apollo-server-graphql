import DataLoader from 'dataloader';
import appConfig from '../config';
import { BaseConnector } from './base';

class CRMConnector extends BaseConnector {
  baseURL = appConfig.CRM_API_BASE_URL;

  urnLoader = new DataLoader(async urns => {
    const customers = await this.getCustomersBulk({ urns });
    return urns.map(
      urn =>
        customers.find(customer => customer.records.indexOf(urn) >= 0) || null,
    );
  });

  idLoader = new DataLoader(async ids => {
    const customers = await this.getCustomersBulk({ ids });
    return ids.map(
      _id => customers.find(customer => customer._id === _id) || null,
    );
  });

  willSendRequest(request) {
    request.headers.set('Authorization', `${this.context.viewer.token}`);
  }

  async getCustomer(id) {
    return this.idLoader.load(id);
  }

  async getCustomerByURN(urn) {
    return this.urnLoader.load(urn);
  }

  async getCustomersBulk(filters) {
    const data = {
      filters: {
        ...filters,
        organization_id: this.context.viewer.organization_id,
      },
    };
    const response = await this.post('/v2/customers-bulk', data);
    return response.customers;
  }
}

export default CRMConnector;
