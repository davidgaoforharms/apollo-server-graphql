import { User, UserRole } from '../models';

class UserService {
  constructor(context) {
    this.context = context;
  }

  async getUsers({ status, dealer_ids, roles }) {
    const data = await this.context.dataSources.userConnector.getUsers(
      null,
      status,
      dealer_ids,
      roles,
    );
    return data.map(u => new User(u));
  }

  async getUserByUsername(username) {
    if (!username) return null;
    const data = await this.context.dataSources.userConnector.getUserByUsername(
      username,
    );
    return new User(data);
  }

  async getUsersByUsername(usernames) {
    const data = await this.context.dataSources.userConnector.getUsersByUsername(
      usernames,
    );
    return data.map(u => new User(u));
  }

  async getUserRoles() {
    const data = await this.context.dataSources.userConnector.getUserRoles();
    return Object.keys(data).map(
      role =>
        new UserRole({
          ...data[role],
          id: role,
        }),
    );
  }

  async getUserRole(roleId) {
    const userRoles = await this.getUserRoles();
    return userRoles.find(role => role.id === roleId);
  }
}

export default UserService;
