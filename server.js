'use strict';

import axios from 'axios';
import 'axios-debug-log';
import compression from 'compression';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import jwt from 'express-jwt';
import * as Sentry from '@sentry/node';
import responseTime from 'response-time';
import PrettyError from 'pretty-error';
import http from 'http';
import { RedisPubSub } from 'graphql-redis-subscriptions';

import config from './config';
import * as connectors from './connectors';
import debug from './debug';
import { typeDefs, resolvers } from './schemas';
import { Viewer } from './authorization';
import { createLoaders } from './loaders';

const prettyError = new PrettyError();

const start = async () => {
  try {
    const jwtPublicKey = await axios
      .get(config.JWT_PUBLIC_KEY_URL)
      .then(response => response.data.public_key);

    const app = express();

    Sentry.init({ dsn: config.SENTRY_DSN });

    // The request handler must be the first middleware on the app
    app.use(Sentry.Handlers.requestHandler());

    app.use(compression());

    app.use('/graphql', jwt({ secret: jwtPublicKey }));

    app.use(function(req, res, next) {
      // Decorate sentry with meta info about the request
      Sentry.configureScope(scope => {
        if (req.user) {
          scope.setUser({
            id: req.user.user_id,
            ip_address:
              req.headers['x-forwarded-for'] || req.connection.remoteAddress,
            ...req.user,
          });
        }
        scope.addEventProcessor(async event =>
          Sentry.Handlers.parseRequest(event, req),
        );
      });
      next();
    });

    app.use(
      responseTime((req, res, time) => {
        debug(
          `${req.method} ${req.originalUrl} ${res.statusCode} (${time} ms)\n`,
        );

        // Log potentially long and damaging queries
        if (time > 5000) {
          Sentry.configureScope(scope => {
            scope.setExtra('Response Time', time);
          });

          Sentry.captureMessage('Query took too long!', 'warning');
        }
      }),
    );

    const pubSub = new RedisPubSub({
      connection: config.REDIS_URL,
    });

    const createContext = req => {
      const viewer = new Viewer({
        ...req.user,
        token: req.headers.authorization,
      });
      return { loaders: createLoaders(viewer), viewer, pubSub };
    };

    const server = new ApolloServer({
      typeDefs,
      resolvers,
      path: '/graphql',
      cors: true,
      introspection: true, // Forced on for Production since we use JWT
      playground: true, // Forced on for Production since we use JWT
      debug: true, // Forced on for Prod to log stacktraces to sentry.
      dataSources: () => {
        return {
          userConnector: new connectors.UserConnector(),
          crmConnector: new connectors.CRMConnector(),
        };
      },
      context: async ({ req, connection }) =>
        connection ? connection.context : createContext(req),
      formatError: error => {
        debug(prettyError.render(error.originalError || error));
        Sentry.captureException(error);
        return error;
      },
      subscriptions: {
        onConnect: ({ authToken }) => {
          let req = { headers: { authorization: authToken } };
          return new Promise(resolve =>
            jwt({ secret: jwtPublicKey })(req, {}, () => resolve(req)),
          ).then(createContext);
        },
      },
    });

    server.applyMiddleware({ app });

    const httpServer = http.createServer(app);
    server.installSubscriptionHandlers(httpServer);

    app.get('/health-check', (req, res) => res.send(200, process.uptime()));

    // The error handler must be before any other error middleware
    app.use(Sentry.Handlers.errorHandler());

    httpServer.listen(config.PORT, () => {
      debug(`馃殌 Running on http://${config.HOST}:${config.PORT}`);
      debug(
        `馃殌 Subscriptions ready at ws://localhost:${config.PORT}${
          server.subscriptionsPath
        }`,
      );
    });
  } catch (e) {
    debug(e);
    throw e;
  }

  process.on('uncaughtException', function(error) {
    Sentry.captureException(error);
    debug('UNCAUGHT EXCEPTION ');
    debug("[Inside 'uncaughtException' event] " + error.stack || error.message);
  });
};

start();