'use strict';

import DataLoader from 'dataloader';
import { orderBy } from 'lodash';

import connector from '../connectors/crm';
import { Note } from './note';
import { CustomerVehicle } from './customerVehicle';

export class CustomerAlertSummaryAssignees {
  constructor(data) {
    this.username = data.username;
    this.count = data.count;
  }
}

export class Customer {
  constructor(data) {
    this.data = data;
    this._id = data._id;
    this.address = data.address;
    this.alerts = data.alerts || [];
    this.cell_phone = data.cell_phone;
    this.company_name = data.company_name;
    this.city = data.city;
    this.created = data.created;
    this.email_history = data.email_history || [];
    this.emails = data.emails || [];
    this.first_name = data.first_name;
    this.fullname = data.fullname;
    this.gender = data.gender;
    this.gocards = data.gocards || [];
    this.home_phone = data.home_phone;
    this.ignore_alerts_for_dealer_until = data.ignore_alerts_for_dealer_until;
    this.last_name = data.last_name;
    this.organization_id = data.organization_id;
    this.postal_code = data.postal_code;
    this.primary_phone = data.phone;
    this.province = data.province;
    this.records = data.records || [];
    this.salutation = data.salutation;
    this.updated = data.updated;
    this.work_phone = data.work_phone;
    this.assigned_salespeople = data.assigned_salespeople || [];
  }

  get assignees() {
    return this.assigned_salespeople.map(asp => asp.username);
  }

  get current_vehicle() {
    return this.vehicles[0];
  }

  get past_vehicles() {
    let vehicles = this.data.past_vehicles || [];
    vehicles = orderBy(vehicles, ['date_purchased']);
    return vehicles.map(v => new CustomerVehicle(v));
  }

  get primary_email() {
    const primaryEmail = this.emails.filter(e => e.primary === true)[0];
    return primaryEmail ? primaryEmail.email : null;
  }

  get vehicles() {
    let vehicles = this.data.vehicles || [];
    vehicles = orderBy(vehicles, ['date_purchased']);
    return vehicles.map(v => new CustomerVehicle(v));
  }

  static async attachAppraisal(context, id, appraisalId) {
    // TODO: Can update Customer permission check
    const urn = `smi.appraisals:appraisal:${appraisalId}`;
    const data = await connector.api.attachRecord(id, urn);
    return data ? new Customer(data) : null;
  }

  static createLoader(viewer) {
    return new DataLoader(
      async ids => {
        const customers = await connector.api.getCustomersBulk(viewer.token, {
          filters: {
            organization_id: viewer.organization_id,
            ids,
          },
        });
        return ids.map(id => customers.find(c => c._id === id) || null);
      },
      { maxBatchSize: 100 },
    );
  }

  static async createCustomer(context, data) {
    data.organization_id = context.viewer.data.organization_id;
    const customer = await connector.api.createCustomer(
      context.viewer.token,
      data,
    );
    return new Customer(customer);
  }

  static async getOne(context, id) {
    if (!id) {
      return null;
    }
    const data = await context.loaders.customerLoader.load(id);
    return data ? new Customer(data) : null;
  }

  static async getMany(context, params = {}) {
    if (!params.organization_id)
      params.organization_id = context.viewer.data.organization_id;
    const customers = await connector.api.getCustomers(
      context.viewer.token,
      params,
    );
    return customers.map(data => new Customer(data));
  }

  async unassignSalesperson(context, dealerID) {
    const customer = await connector.api.unassignSalespeopleFromCustomerForDealer(
      context.viewer.token,
      this._id,
      dealerID,
    );
    return customer ? new Customer(customer) : null;
  }

  async update(context, data) {
    const customer = await connector.api.updateCustomer(
      context.viewer.token,
      this._id,
      data,
    );
    return customer ? new Customer(customer) : null;
  }

}
