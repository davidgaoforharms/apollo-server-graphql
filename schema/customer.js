import { gql } from 'apollo-server-express';

import * as models from '../models';
import { DealerService, UserService } from '../services';

export const typeDefs = gql`
  type Customer {
    _id: ID!
    address: String
    alerts: [CustomerAlert]!
    appointments(
      attendees: [String]
      created_by: [String]
      created_from: Date
      created_until: Date
      dealer_ids: [Int]
      statuses: [String]
      start_date_from: Date
      start_date_until: Date
      types: [String]
    ): [Appointment]!
    assignees: [String]!
    cell_phone: String
    company_name: String
    city: String
    created: Date
    current_vehicle: CustomerVehicle
    email_history: [EmailHistory]!
    emails: [CustomerEmail]!
    first_name: String
    fullname: String
    gender: String
    gocards: [GoCard]!
    home_phone: String
    ignore_alerts_for_dealer_until: JSON
    last_name: String
    leads: [Lead]
    notes(from: Date, until: Date): [Note]!
    opportunities(
      statuses: [Int]
      dealer_ids: [Int]
      assignee: String
    ): [Opportunity]!
    organization_id: String!
    past_vehicles: [CustomerVehicle]!
    postal_code: String
    prescreens: [Prescreen]!
    primary_email: String
    primary_phone: String
    province: String
    records: [String]!
    salutation: String
    tasks(
      assignee: String
      dealer_ids: [Int]
      due_from: Date
      due_until: Date
      statuses: [Int]
    ): [Task]!
    updated: Date
    vehicles: [CustomerVehicle]!
    work_phone: String
  }

  input CustomerVehicleInput {
    make: String
    model: String
    trim: String
    vin: String
    year: Int
  }

  enum SortCustomerBy {
    city_asc
    city_desc
    created_asc
    created_desc
    first_name_asc
    first_name_desc
    last_name_asc
    last_name_desc
    province_asc
    province_desc
  }

  type AlertType {
    type: String
    name: String
  }

  type BookValue {
    condition: String
    updated: Date
    value: Int
  }

  type CustomerAlert {
    type: String
    dealer_id: Int
    dealer: Dealer
    vin: String
    created: Date
  }

  type CustomerEmail {
    consent: String
    email: String!
    primary: Boolean
  }

  type CustomerVehicle {
    _id: ID!
    acode: String
    balance_owing: Float
    balloon_amount: Float
    body_type: String
    book_value: BookValue
    cash_price: Float
    date_purchased: Date
    deal_number: String
    deal_type: String
    equity: Float
    finance_amount: Float
    finance_term: String
    first_payment_date: Date
    last_payment_date: Date
    last_service: ServiceHistory
    lender: String
    make: String
    model: String
    odometer: Int
    payment_amount: Float
    payment_frequency: String
    payment_type: String
    purchase_mbi_cost: Float
    purchased_at_dealer_id: Int
    purchased_at: Dealer
    rate: Float
    sales_deal_type: String
    sales_history_urn: String
    sell_rate_apr: Float
    service_history: [ServiceHistory]!
    trim: String
    vin: String
    year: Int
  }

  type EmailHistory {
    username: ID
    user: User
    from_name: String
    to_address: String
    from_address: String
    s3_key: String
    body_plain: String
    mapping_id: ID # Not sure what this ID points to....
    is_outgoing: Boolean
    to_name: String
    date: Date
    dealer_id: Int
    dealer: Dealer
    subject: String
  }

  extend type Query {
    alertTypes: [AlertType]!
    customer(_id: ID!): Customer
    customers(
      organization_id: String
      assignees: [String]
      dealer_ids: [Int]
      ids: [ID]
      urns: [ID]
      type: Int
      search: String
      sort_by: [SortCustomerBy]
      page: Int
      page_size: Int
    ): [Customer]!
    customersWithAlerts(
      assignees: [String]
      dealer_id: Int
      exclude_assignees: [String]
      limit: Int
      offset: Int
      type: [String]
    ): [Customer]!
    customersWithAlertsSummary(
      assignees: [String]
      type: [String]
      dealer_id: Int
    ): [AlertSummary]!
  }

  extend type Mutation {
    assignSalespersonToCustomer(
      _id: ID!
      input: AssignSalespersonInput!
    ): Customer
    createCustomer(input: CreateCustomerInput!): Customer
    createVehicleForCustomer(_id: ID!, input: CustomerVehicleInput!): Customer
    deletePastVehicleFromCustomer(_id: ID!, vehicle_id: ID!): Customer
    detachVehicleFromCustomer(_id: ID!, vehicle_id: ID!): Customer
    dismissAlertsForCustomer(_id: ID!, input: DismissAlertsInput!): Customer
    replaceCustomerVehicle(
      _id: ID!
      vehicle_id: ID!
      input: CustomerVehicleInput!
    ): Customer
    restoreCustomerVehicle(_id: ID!, past_vehicle_id: ID!): Customer
    unassignSalespersonFromCustomer(_id: ID!, dealer_id: ID!): Customer
    updateCustomer(_id: ID!, input: UpdateCustomerInput!): Customer
    updateCustomerVehicle(
      _id: ID!
      vehicle_id: ID!
      input: CustomerVehicleInput!
    ): Customer
  }
`;

export const resolvers = {
    Customer: {
        appointments: ({ _id }, args, context) =>
            models.Appointment.getManyForCustomerId(context, _id, args),
        leads: async ({ _id: customer_id }, _, context) =>
            (await models.Lead.getMany(context, {
                query: { customer_id },
            })).results,
        opportunities: async ({ _id: customer_id }, args, context) =>
            (await models.Opportunity.getMany(context, {
                ...args,
                customer_ids: [customer_id],
            })).results,
        prescreens: ({ _id }, args, context) =>
            models.Prescreen.getManyForCustomerId(context, _id, args),
        tasks: ({ _id }, args, context) =>
            models.Task.getManyForCustomerID(context, _id, args),
    },
    Query: {
        alertTypes: (_, args, context) => models.Customer.getAlertTypes(context),
        customer: (_, { _id }, context) => models.Customer.getOne(context, _id),
        customers: (_, args, context) => models.Customer.getMany(context, args),
    },
    Mutation: {
        assignSalespersonToCustomer: async (_, { _id, input }, context) =>
            await new models.Customer({ _id }).assignSalesperson(context, input),
        createCustomer: async (_, data, context) =>
            await models.Customer.createCustomer(context, data.input),
        createVehicleForCustomer: async (_, { _id, input }, context) =>
            await new models.Customer({ _id }).createVehicle(context, input),
        deletePastVehicleFromCustomer: async (_, { _id, vehicle_id }, context) =>
            await new models.Customer({ _id }).deleteVehicle(context, vehicle_id),
        detachVehicleFromCustomer: async (_, { _id, vehicle_id }, context) =>
            await new models.Customer({ _id }).detachVehicle(context, vehicle_id),
        dismissAlertsForCustomer: async (_, { _id, input }, context) =>
            await new models.Customer({ _id }).dismissAlerts(context, input),
        replaceCustomerVehicle: async (_, { _id, vehicle_id, input }, context) =>
            await new models.Customer({ _id }).updateVehicle(
                context,
                vehicle_id,
                input,
                true,
            ),
        updateCustomer: async (_, { _id, input }, context) =>
            await new models.Customer({ _id }).update(context, input),
        updateCustomerVehicle: async (_, { _id, vehicle_id, input }, context) =>
            await new models.Customer({ _id }).updateVehicle(
                context,
                vehicle_id,
                input,
                false,
            ),
    },
};